module gitlab.com/rackn/ring

go 1.20

require (
	golang.org/x/exp v0.0.0-20230315142452-642cacee5cc0
	golang.org/x/sync v0.1.0
)
