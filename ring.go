package ring

import (
	"golang.org/x/sync/semaphore"
	"sync"
)

// B is the basic type for a generic ringbuffer.  It contains the bare minimum
// amount of information needed to keep track of the items being buffered.
// In particular, it is not concurrency safe.
type B[T any] struct {
	head    int // Position in entries where we should start inserting values.
	tail    int // Position in entries of the least recently inserted value.
	entries []T
}

// spaceFree calculates how much free space the buffer has.
func (b *B[T]) spaceFree() int {
	if b.tail == -1 {
		return len(b.entries)
	}
	if b.tail == b.head {
		// if tail and head refer to the same entry, the buffer is full
		return 0
	}
	if b.tail > b.head {
		return b.tail - b.head
	}
	return len(b.entries) - (b.head - b.tail)
}

func (b *B[T]) spaceUsed() int {
	return len(b.entries) - b.spaceFree()
}

// NewB returns a new basic ring buffer.
func NewB[T any](size int) *B[T] {
	return &B[T]{entries: make([]T, size), head: -1, tail: -1}
}

// Pop attemps to remove up to len(res) items from the
// buffer.  It returns the number of items that
// were removed from the buffer.
func (b *B[T]) Pop(res []T) (count int) {
	if b.head == -1 || len(res) == 0 {
		return
	}
	for count < len(res) {
		var empty T
		res[count] = b.entries[b.tail]
		b.entries[b.tail] = empty
		b.tail++
		count++
		if b.tail == len(b.entries) {
			b.tail = 0
		}
		if b.tail == b.head {
			break
		}
	}
	if b.tail == b.head {
		b.tail = -1
		b.head = -1
	}
	return
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Push attempts to add items to the buffer.  It will not
// overflow, and returns the number of items that were added
// to the buffer.
func (b *B[T]) Push(items ...T) (count int) {
	if len(items) == 0 {
		return
	}
	if b.head == -1 {
		count = copy(b.entries, items)
		b.tail = 0
		b.head = count
		if b.head == len(b.entries) {
			b.head = 0
		}
		return
	} else if b.head == b.tail {
		return
	}
	for i := range items {
		b.entries[b.head] = items[i]
		b.head++
		count++
		if b.head == len(b.entries) {
			b.head = 0
		}
		if b.head == b.tail {
			break
		}
	}
	return
}

// Buf is a concurrency-safe buffer.
type Buf[T any] struct {
	*B[T]
	mu *sync.Mutex
}

func NewBuf[T any](size int) *Buf[T] {
	return &Buf[T]{
		B:  NewB[T](size),
		mu: &sync.Mutex{},
	}
}

func (b *Buf[T]) Push(items ...T) int {
	b.mu.Lock()
	defer b.mu.Unlock()
	return b.B.Push(items...)
}

func (b *Buf[T]) Pop(res []T) int {
	b.mu.Lock()
	defer b.mu.Unlock()
	return b.B.Pop(res)
}

// Consumer is a buffer where items pushed into it are consumed by a bounded number
// of workers.  Any items pushed to the buffer in excess of maxParallel will wait until
// a slot is available to process the item.  Items are removed from the buffer when a
// worker picks them up.
type Consumer[T any] struct {
	b    *Buf[T]
	fn   func(T)
	sema *semaphore.Weighted
}

func NewConsumer[T any](size int, maxParallel int64, consumer func(T)) *Consumer[T] {
	return &Consumer[T]{
		b:    NewBuf[T](size),
		fn:   consumer,
		sema: semaphore.NewWeighted(maxParallel),
	}
}

func (c *Consumer[T]) run(v T) {
	defer c.kick()
	defer c.sema.Release(1)
	c.fn(v)
}

func (c *Consumer[T]) kick() {
	c.b.mu.Lock()
	defer c.b.mu.Unlock()
	used := c.b.spaceUsed()
	if used == 0 {
		return
	}
	count := 0
	for used > 0 {
		if !c.sema.TryAcquire(1) {
			break
		}
		count++
		used--
	}
	if count == 0 {
		return
	}
	vals := make([]T, count)
	c.b.B.Pop(vals)
	for count = range vals {
		go func(i int) {
			c.run(vals[i])
		}(count)
	}
}

func (c *Consumer[T]) Push(items ...T) int {
	defer c.kick()
	return c.b.Push(items...)
}

func (c *Consumer[T]) Dequeue() {
	c.b.mu.Lock()
	defer c.b.mu.Unlock()
	if c.b.B.tail == -1 {
		return
	}
	for {
		var v T
		c.b.B.entries[c.b.B.tail] = v
		c.b.B.tail++
		if c.b.B.tail == len(c.b.B.entries) {
			c.b.B.tail = 0
		}
		if c.b.B.tail == c.b.B.head {
			break
		}
	}
	c.b.B.tail = -1
	c.b.B.head = -1
}
