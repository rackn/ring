package ring

import (
	"golang.org/x/exp/slices"
	"testing"
)

func (b *B[T]) livePart() []T {
	res := make([]T, b.spaceUsed())
	if b.tail == -1 {
		return res
	}
	start := b.tail
	for i := range res {
		res[i] = b.entries[start]
		start++
		if start == len(b.entries) {
			start = 0
		}
	}
	return res
}

func TestRing(t *testing.T) {
	scratch := [5]int{}
	vals := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
	r := NewB[int](5)
	if v := r.Push(vals...); v != 5 {
		t.Errorf("Wanted to add 5, added %d", v)
	} else if !slices.Equal(vals[:5], r.livePart()) {
		t.Errorf("Live part not what is expected")
	}
	if v := r.Pop(scratch[:]); v != 5 {
		t.Errorf("Wanted to remove 5, removed %d", v)
	} else if !slices.Equal(vals[:0], r.livePart()) {
		t.Errorf("Live part not what is expected")
	}
	r.Push(vals[5:]...)
	r.Pop(scratch[:2])
	if !slices.Equal(vals[7:10], r.livePart()) {
		t.Errorf("Live part not what is expected")
	}
	if v := r.Push(vals...); v != 2 {
		t.Errorf("Wanted to add 2, added %d", v)
	}
	t.Logf("ok")
	r.Pop(scratch[:])
	r.Push(vals...)
	r.Pop(scratch[:4])
	r.Push(vals[5:7]...)
	if !slices.Equal(vals[4:7], r.livePart()) {
		t.Errorf("Live part not what is expected")
	}
}
